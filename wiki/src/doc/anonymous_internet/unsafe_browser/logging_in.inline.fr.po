# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2020-06-25 02:00+0000\n"
"PO-Revision-Date: 2020-11-04 19:31+0000\n"
"Last-Translator: Corl3ss <corl3ss@corl3ss.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 3.8\n"

#. type: Plain text
msgid "To log in to a captive portal:"
msgstr ""

#. type: Bullet: '1. '
msgid "Try visiting any website using the *Unsafe Browser*."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"   Choose a website that is common in your location, for example a search\n"
"   engine or news site.\n"
msgstr ""
"   Choisissez un site web habituel selon votre localisation, par exemple un "
"moteur\n"
"   de recherche ou un site d'actualités.\n"

#. type: Bullet: '1. '
msgid "You should be redirected to the captive portal instead of the website."
msgstr ""
"Vous devriez être redirigé vers le portail captif à la place du site web."

#. type: Bullet: '1. '
msgid "After you logged in to the captive portal, Tor should start."
msgstr "Après votre connexion au portail captif, Tor devrait démarrer."

#. type: Bullet: '1. '
msgid "After Tor is ready, close the *Unsafe Browser*."
msgstr "Une fois Tor prêt, fermez le *Navigateur non sécurisé*."

#. type: Plain text
#, no-wrap
msgid "   You can use *Tor Browser* and any other application as usual.\n"
msgstr ""
"   Vous pouvez utiliser le *Navigateur Tor* et tout autre application comme "
"d'habitude.\n"
