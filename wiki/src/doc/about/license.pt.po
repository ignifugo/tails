# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2020-12-09 16:43+0000\n"
"PO-Revision-Date: 2020-04-30 21:35+0000\n"
"Last-Translator: drebs <drebs@riseup.net>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 3.5.1\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"License and source code distribution\"]]\n"
msgstr "[[!meta title=\"Licença e distribuição do código fonte\"]]\n"

#. type: Plain text
msgid ""
"Tails is [Free Software](https://www.gnu.org/philosophy/free-sw.html): you "
"can download, use, and share it with no restrictions."
msgstr ""
"Tails é [Software Livre](https://www.gnu.org/philosophy/free-sw.pt-br.html): "
"você pode baixar, usar e compartilhar sem restrições."

#. type: Plain text
msgid ""
"The Tails source code is released under the GNU/GPL (version 3 or above) and "
"is Copyright (C) Tails developers <tails@boum.org>.  Any exception to this "
"rule is documented either below or in the affected source file."
msgstr ""
"O código fonte do Tails é licenciado sob a GNU/GPL (versão 3 ou superior) e "
"os Direitos Autorais (C) são de Tails developers <tails@boum.org>. Qualquer "
"exceção a essa regra está documentada abaixo ou dentro do arquivo de código "
"fonte afetado."

#. type: Plain text
msgid ""
"However, Tails includes non-free firmware in order to work on as much "
"hardware as possible."
msgstr ""
"Apesar disso, o Tails inclui arquivos de firmware não livres para poder "
"funcionar no maior número possível de hardwares."

#. type: Title =
#, no-wrap
msgid "Source code"
msgstr ""

#. type: Plain text
msgid ""
"Most of the software included in Tails is taken directly from upstream "
"Debian packages and is neither modified nor recompiled by Tails."
msgstr ""
"A maior parte dos programas inclusos no Tails são tirados diretamente dos "
"pacotes do Debian e não são nem modificados nem recompilados por nós."

#. type: Bullet: '- '
msgid ""
"The source code of software that is specific to Tails is available in [[our "
"Git repositories|contribute/git]]."
msgstr ""
"O código fonte dos programas que são específicos do Tails está disponível em "
"[[nossos repositórios Git|contribute/git]]."

#. type: Bullet: '- '
#, fuzzy
#| msgid ""
#| "The source code of the Debian packages included in Tails is available in "
#| "the [APT snapshot](http://tagged.snapshots.deb.tails.boum.org/)  that we "
#| "created for that version of Tails."
msgid ""
"The source code of the Debian packages included in Tails is available in the "
"[APT snapshot](https://tagged.snapshots.deb.tails.boum.org/)  that we "
"created for that version of Tails."
msgstr ""
"Os códigos-fonte dos pacotes do Debian que são incluídos no Tails estão "
"disponíveis em um [snapshot APT](http://tagged.snapshots.deb.tails.boum."
"org/) criado por nós para aquela versão do Tails."

#. type: Plain text
#, no-wrap
msgid ""
"- The [[!tails_gitweb\n"
"  config/chroot_local-includes/usr/share/tails/chroot-browsers/unsafe-browser/extensions/red-2.0-an+fx.xpi\n"
"  desc=\"red theme\"]] used by Tails' _Unsafe Browser_ was downloaded\n"
"  from Mozilla add-ons website\n"
"  (<https://addons.mozilla.org/en-US/firefox/addon/simplyred/>):\n"
"  - license: Creative Commons Attribution 3.0\n"
"  - author: [Firefox user 14030863](https://addons.mozilla.org/en-US/firefox/user/14030863/)\n"
msgstr ""

#. type: Title =
#, no-wrap
msgid "Website"
msgstr ""

#. type: Plain text
#, fuzzy
#| msgid ""
#| "This website uses several images with various copyright, licenses, "
#| "trademarks and terms of distributions:"
msgid ""
"This website uses several images with distinct copyright, licenses, "
"trademarks and distribution terms:"
msgstr ""
"Este site usa várias imagens com distintos direitos de cópia, licenças, "
"marcas e termos de distribuição:"

#. type: Bullet: '- '
msgid ""
"The Tails logo is based on [[USB|http://thenounproject.com/term/usb/23873/]] "
"by Ilsur Aptukov from the Noun Project."
msgstr ""

#. type: Plain text
#, fuzzy
#| msgid "Debian logo: Copyright (c) 1999 Software in the Public Interest."
msgid "- Debian logo: Copyright (c) 1999 Software in the Public Interest."
msgstr "Logo do Debian: Copyright (c) 1999 Software no Interesse Público"

#. type: Bullet: '- '
msgid ""
"Onion logo: registered trademark of The Tor Project, Inc.; the Tails project "
"is authorized to use it under certain conditions; licensed under Creative "
"Commons Attribution 3.0 United States License."
msgstr ""

#. type: Plain text
msgid ""
"- [Forge](https://github.com/digitalbazaar/forge/), copyright Digital "
"Bazaar, Inc."
msgstr ""

#. type: Bullet: '- '
msgid ""
"The drawings on our [[Home|index]] and [[About|about]] pages are licensed "
"under the [Creative Commons Attribution-ShareAlike 4.0 International License]"
"(https://creativecommons.org/licenses/by-sa/4.0/), copyright [Andrés "
"Fernández Cordón](https://anhdr.es/)  ([[source|lib/drawings.zip]] for Adobe "
"Illustrator)."
msgstr ""

#. type: Bullet: '- '
msgid ""
"All the [[drawings of our personas|contribute/personas]] are licensed under "
"the [Creative Commons Attribution-ShareAlike 4.0 International License]"
"(https://creativecommons.org/licenses/by-sa/4.0/), copyright Thomas Verguet."
msgstr ""

#. type: Bullet: '- '
msgid ""
"The [[Boot Menu animation|install/win/usb#animation]] is licensed under the "
"[Creative Commons Attribution-ShareAlike 4.0 International License](https://"
"creativecommons.org/licenses/by-sa/4.0/), copyright [Odile Carabantes]"
"(https://studiomoare.com/) and [Enso Studio](https://www.ensostudio.tv/)  "
"([source](https://gitlab.tails.boum.org/tails/ux/-/raw/master/boot%20menu"
"%20key/animation.rar)  for Adobe Illustrator and Adobe Premiere)."
msgstr ""

#. type: Plain text
msgid "- Icons from [[The Noun Project|http://thenounproject.com/]]:"
msgstr ""

#. type: Bullet: '  - '
msgid ""
"[[Announcement|http://thenounproject.com/term/announcement/1186/]]: Creative "
"Commons - Attribution, by Olivier Guin."
msgstr ""

#. type: Bullet: '  - '
msgid ""
"[[Code|http://thenounproject.com/term/code/18033/]]: Creative Commons — "
"Attribution, by Azis."
msgstr ""

#. type: Bullet: '  - '
msgid ""
"[[Pen|http://thenounproject.com/term/pen/18907/]]: Creative Commons — "
"Attribution, by factor[e] design initiative."
msgstr ""

#. type: Bullet: '  - '
msgid ""
"[[Loan|http://thenounproject.com/term/loan/19538/]]: Public Domain, by "
"Rohith M S."
msgstr ""

#. type: Bullet: '  - '
msgid ""
"[[User|http://thenounproject.com/term/user/419/]]: Creative Commons — "
"Attribution, by Edward Boatman."
msgstr ""

#. type: Bullet: '  - '
msgid ""
"[[Translation|http://thenounproject.com/term/translation/5735/]]: Creative "
"Commons — Attribution, by Joe Mortell."
msgstr ""

#. type: Bullet: '  - '
msgid ""
"[[Gears|http://thenounproject.com/term/gears/8949/]]: Creative Commons — "
"Attribution, by Cris Dobbins."
msgstr ""

#. type: Bullet: '  - '
msgid ""
"[[Graphic Design|http://thenounproject.com/term/graphic_design/9198/]]: "
"Creative Commons — Attribution, by Cornelius Danger."
msgstr ""

#. type: Bullet: '  - '
msgid ""
"[[Check Mark|https://thenounproject.com/term/check/4489/]]: Public Domain, "
"by Julian Norton."
msgstr ""

#. type: Bullet: '  - '
msgid ""
"[[Delete|https://thenounproject.com/term/delete/49691/]]: Creative Commons — "
"Attribution, by Kervin Markle."
msgstr ""

#. type: Bullet: '  - '
msgid ""
"[[Play|https://thenounproject.com/term/play/152052]]: Creative Commons — "
"Attribution, by Zech Nelson."
msgstr ""

#. type: Bullet: '  - '
msgid ""
"[[Pause|https://thenounproject.com/term/pause/152046]]: Creative Commons — "
"Attribution, by Zech Nelson."
msgstr ""

#. type: Bullet: '  - '
msgid ""
"[[USB|https://thenounproject.com/term/usb/28901]]: Creative Commons — "
"Attribution, by Wilson Joseph."
msgstr ""

#. type: Bullet: '  - '
msgid ""
"[[Time|https://thenounproject.com/term/time/6732]]: Creative Commons — "
"Attribution, by Richard de Vos."
msgstr ""

#. type: Bullet: '  - '
msgid ""
"[[Printer|https://thenounproject.com/term/printer/146674]]: Creative Commons "
"— Attribution, by Diego Naive."
msgstr ""

#. type: Bullet: '  - '
msgid ""
"[[Smart Phones|https://thenounproject.com/term/smartphone/25790]]: Creative "
"Commons — Attribution, by Pham Thi Dieu Linh."
msgstr ""

#. type: Bullet: '  - '
msgid ""
"[[Laptop|https://thenounproject.com/term/laptop/6729]]: Public Domain, by "
"Jean Yashu."
msgstr ""

#. type: Bullet: '  - '
msgid ""
"[[Love|https://thenounproject.com/term/love/218846/]]: Creative Commons — "
"Attribution, by Thomas Helbig."
msgstr ""

#. type: Bullet: '  - '
msgid ""
"[[Survey|https://thenounproject.com/term/survey/1296072]]: Creative Commons "
"— Attribution, by unlimicon."
msgstr ""

#. type: Bullet: '  - '
msgid ""
"[[Info|https://thenounproject.com/term/info/808461]]: Creative Commons — "
"Attribution, by icongeek."
msgstr ""

#, fuzzy
#~| msgid ""
#~| "  - The Tails logo is based on [[USB|http://thenounproject.com/term/"
#~| "usb/23873/]]\n"
#~| "    by Ilsur Aptukov from the Noun Project.\n"
#~| "  - Debian logo: Copyright (c) 1999 Software in the Public Interest.\n"
#~| "  - Onion logo: registered trademark of The Tor Project, Inc.; the "
#~| "Tails\n"
#~| "project is authorized to use it under certain conditions; licensed "
#~| "under\n"
#~| "Creative Commons Attribution 3.0 United States License.\n"
#~| "  - Icons from [[The Noun Project|http://thenounproject.com/]]:\n"
#~| "    - [[Announcement|http://thenounproject.com/term/"
#~| "announcement/1186/]]:\n"
#~| "      Creative Commons - Attribution, by Olivier Guin.\n"
#~| "    - [[Code|http://thenounproject.com/term/code/18033/]]: Creative "
#~| "Commons —\n"
#~| "      Attribution, by Azis.\n"
#~| "    - [[Pen|http://thenounproject.com/term/pen/18907/]]: Creative "
#~| "Commons —\n"
#~| "      Attribution, by factor[e] design initiative.\n"
#~| "    - [[Loan|http://thenounproject.com/term/loan/19538/]]: Public "
#~| "Domain, by\n"
#~| "      Rohith M S.\n"
#~| "    - [[User|http://thenounproject.com/term/user/419/]]: Creative "
#~| "Commons —\n"
#~| "      Attribution, by Edward Boatman.\n"
#~| "    - [[Translation|http://thenounproject.com/term/translation/5735/]]: "
#~| "Creative\n"
#~| "      Commons — Attribution, by Joe Mortell.\n"
#~| "    - [[Gears|http://thenounproject.com/term/gears/8949/]]: Creative "
#~| "Commons —\n"
#~| "      Attribution, by Cris Dobbins.\n"
#~| "    - [[Graphic Design|http://thenounproject.com/term/"
#~| "graphic_design/9198/]]:\n"
#~| "      Creative Commons — Attribution, by Cornelius Danger.\n"
#~ msgid ""
#~ "  - The Tails logo is based on [[USB|http://thenounproject.com/term/"
#~ "usb/23873/]]\n"
#~ "    by Ilsur Aptukov from the Noun Project.\n"
#~ "  - Debian logo: Copyright (c) 1999 Software in the Public Interest.\n"
#~ "  - Onion logo: registered trademark of The Tor Project, Inc.; the Tails\n"
#~ "project is authorized to use it under certain conditions; licensed under\n"
#~ "Creative Commons Attribution 3.0 United States License.\n"
#~ "  - Icons from [[The Noun Project|http://thenounproject.com/]]:\n"
#~ "    - [[Announcement|http://thenounproject.com/term/"
#~ "announcement/1186/]]:\n"
#~ "      Creative Commons - Attribution, by Olivier Guin.\n"
#~ "    - [[Code|http://thenounproject.com/term/code/18033/]]:\n"
#~ "      Creative Commons — Attribution, by Azis.\n"
#~ "    - [[Pen|http://thenounproject.com/term/pen/18907/]]:\n"
#~ "      Creative Commons — Attribution, by factor[e] design initiative.\n"
#~ "    - [[Loan|http://thenounproject.com/term/loan/19538/]]:\n"
#~ "      Public Domain, by Rohith M S.\n"
#~ "    - [[User|http://thenounproject.com/term/user/419/]]:\n"
#~ "      Creative Commons — Attribution, by Edward Boatman.\n"
#~ "    - [[Translation|http://thenounproject.com/term/translation/5735/]]:\n"
#~ "      Creative Commons — Attribution, by Joe Mortell.\n"
#~ "    - [[Gears|http://thenounproject.com/term/gears/8949/]]:\n"
#~ "      Creative Commons — Attribution, by Cris Dobbins.\n"
#~ "    - [[Graphic Design|http://thenounproject.com/term/"
#~ "graphic_design/9198/]]:\n"
#~ "      Creative Commons — Attribution, by Cornelius Danger.\n"
#~ "    - [[Check Mark|https://thenounproject.com/term/check/4489/]]:\n"
#~ "      Public Domain, by Julian Norton.\n"
#~ "    - [[Delete|https://thenounproject.com/term/delete/49691/]]:\n"
#~ "      Creative Commons — Attribution, by Kervin Markle.\n"
#~ "    - [[Play|https://thenounproject.com/term/play/152052]]:\n"
#~ "      Creative Commons — Attribution, by Zech Nelson.\n"
#~ "    - [[Pause|https://thenounproject.com/term/pause/152046]]:\n"
#~ "      Creative Commons — Attribution, by Zech Nelson.\n"
#~ "    - [[USB|https://thenounproject.com/term/usb/28901]]:\n"
#~ "      Creative Commons — Attribution, by Wilson Joseph.\n"
#~ "    - [[Time|https://thenounproject.com/term/time/6732]]:\n"
#~ "      Creative Commons — Attribution, by Richard de Vos.\n"
#~ "    - [[Printer|https://thenounproject.com/term/printer/146674]]:\n"
#~ "      Creative Commons — Attribution, by Diego Naive.\n"
#~ "    - [[Smart Phones|https://thenounproject.com/term/smartphone/25790]]:\n"
#~ "      Creative Commons — Attribution, by Pham Thi Dieu Linh.\n"
#~ "    - [[Laptop|https://thenounproject.com/term/laptop/6729]]:\n"
#~ "      Public Domain, by Jean Yashu.\n"
#~ "    - [[Love|https://thenounproject.com/term/love/218846/]]:\n"
#~ "      Creative Commons — Attribution, by Thomas Helbig.\n"
#~ "    - [[Survey|https://thenounproject.com/term/survey/1296072]]:\n"
#~ "      Creative Commons — Attribution, by unlimicon.\n"
#~ "    - [[Info|https://thenounproject.com/term/info/808461]]:\n"
#~ "      Creative Commons — Attribution, by icongeek]].\n"
#~ msgstr ""
#~ "  - O logotipo do Tails logo é baseado em [[USB|http://thenounproject.com/"
#~ "term/usb/23873/]]\n"
#~ "    por Ilsur Aptukov do Noun Project.\n"
#~ "  - Logotipo Debian: Copyright (c) 1999 Software de interesse público.\n"
#~ "  - Logotipo Onion: marca registrada do The Tor Project, Inc.; o projeto "
#~ "Tails\n"
#~ "está autorizado a usá-lo sob certas condições; licenciado sob\n"
#~ "Creative Commons Attribution 3.0 United States License.\n"
#~ "  - Ícones do [[The Noun Project|http://thenounproject.com/]]:\n"
#~ "    - [[Announcement|http://thenounproject.com/term/"
#~ "announcement/1186/]]:\n"
#~ "      Creative Commons - Attribution, por Olivier Guin.\n"
#~ "    - [[Code|http://thenounproject.com/term/code/18033/]]: Creative "
#~ "Commons —\n"
#~ "      Attribution, por Azis.\n"
#~ "    - [[Pen|http://thenounproject.com/term/pen/18907/]]: Creative Commons "
#~ "—\n"
#~ "      Attribution, por factor[e] design initiative.\n"
#~ "    - [[Loan|http://thenounproject.com/term/loan/19538/]]: Public Domain, "
#~ "por\n"
#~ "      Rohith M S.\n"
#~ "    - [[User|http://thenounproject.com/term/user/419/]]: Creative Commons "
#~ "—\n"
#~ "      Attribution, por Edward Boatman.\n"
#~ "    - [[Translation|http://thenounproject.com/term/translation/5735/]]: "
#~ "Creative\n"
#~ "      Commons — Attribution, por Joe Mortell.\n"
#~ "    - [[Gears|http://thenounproject.com/term/gears/8949/]]: Creative "
#~ "Commons —\n"
#~ "      Attribution, por Cris Dobbins.\n"
#~ "    - [[Graphic Design|http://thenounproject.com/term/"
#~ "graphic_design/9198/]]:\n"
#~ "      Creative Commons — Attribution, por Cornelius Danger.\n"

#, fuzzy
#~| msgid ""
#~| "Tails is [[Free Software|http://www.gnu.org/philosophy/free-sw.html]] "
#~| "released under the GNU/GPL (version 3 or above)."
#~ msgid "Tails is released under the GNU/GPL (version 3 or above)."
#~ msgstr ""
#~ "Tails é [[Software Livre|http://www.gnu.org/philosophy/free-sw.html]] (em "
#~ "inglês) lançado sob a GNU/GPL (versão 3 ou superior)."

#~ msgid "Distribution of the source code\n"
#~ msgstr "Distribuição do código fonte\n"

#~ msgid ""
#~ "According to the GPL licence (section 3(b) of the GPLv2 and section 6(b) "
#~ "of the GPLv3), complete sources for all Tails releases are available for "
#~ "anyone who requests them, in DVD format, via postal mail, for a nominal "
#~ "charge. If you only require one or two source packages, Tails can work "
#~ "with you to send a copy of individual packages electronically."
#~ msgstr ""
#~ "De acordo com a licença GPL (seção 3(b) da GPLv2 e seção 6(b) da GPLv3, "
#~ "códigos fonte completos para todas as versões do Tails estão disponíveis "
#~ "para qualquer um que os peça, em formato DVD, por correio postal, "
#~ "mediante um pequeno custo. Se você precisa somente de um ou dois pacotes "
#~ "fonte, podemos combinar com você de enviar uma cópia dos pacotes "
#~ "individuais por meios eletrônicos."

#~ msgid ""
#~ "  - USB stick icon: [[source|http://www.openclipart.org/detail/29129]]; "
#~ "waived\n"
#~ "of all copyright and related or neighboring rights under the CC0 PD "
#~ "Dedication;\n"
#~ "[[policies|http://www.openclipart.org/policies]].\n"
#~ "  - CD icon: [[source|http://thenounproject.com/noun/cd/#icon-No255]]; "
#~ "license:\n"
#~ "attribution; designer: The Noun Project.\n"
#~ "  - Onion Logo: registered trademark of The Tor Project, Inc.; the Tails\n"
#~ "project is authorized to use it under certain conditions; licensed under\n"
#~ "Creative Commons Attribution 3.0 United States License.\n"
#~ msgstr ""
#~ "  - Ícone da memória USB: [[fonte|http://www.openclipart.org/"
#~ "detail/29129]]; renunciado\n"
#~ "de todo copyright e direitos afins através [[desta política|http://www."
#~ "openclipart.org/policies]]  de CC0 PD Dedication (Domínio Público);\n"
#~ "  - Ícone de CD: [[fonte|http://thenounproject.com/noun/cd/#icon-No255]]; "
#~ "licença:\n"
#~ "atribuída; designer: The Noun Project.\n"
#~ "  - Logo Onion: marca registrada do Projeto Tor, Inc.; o projeto Tails\n"
#~ "está autorizado a usá-lo sob certas condições; licenciado sobre\n"
#~ "Creative Commons Attribution 3.0 United States License.\n"
